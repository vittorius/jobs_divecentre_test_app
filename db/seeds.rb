# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin_role = Role.create({ name: Role::ADMIN })
Role.create({ name: Role::USER })

u = User.create({
  email: 'admin@gmail.com',
  name: 'Admin',
  password: 'password',
  password_confirmation: 'password',
  remember_me: true
})
u.roles = [admin_role]
u.save

u = User.create({
    email: 'user@gmail.com',
    name: 'User',
    password: 'password',
    password_confirmation: 'password',
    remember_me: true
})
u.save