class MakeFkMoreStrict < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.change :user_id, :integer, :null => false
    end

    change_table :comments do |t|
      t.change :post_id, :integer, :null => false
    end
  end
end
