class AddHeaderToPosts < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.string :header
    end
  end
end
