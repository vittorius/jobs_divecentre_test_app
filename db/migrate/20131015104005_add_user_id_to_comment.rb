class AddUserIdToComment < ActiveRecord::Migration
  def change
    change_table :comments do |t|
      t.integer :user_id,  null: false, default: 0
    end
  end
end
