# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $('.show-comment-edit').click (e) ->
    e.preventDefault()

    link = $(this)
    id = link.attr('id')
    if id
      md = id.match(/show\-comment\-edit\-(\d+)/)
      if md
        id = md[1]
        $('#comment-text-' + id).toggle();
        $('#comment-edit-form-' + id).toggle();

        linkText = $('#show-comment-edit-' + id).text();
        if linkText == 'Edit'
          $('#show-comment-edit-' + id).text('Cancel Edit')
        else
          $('#show-comment-edit-' + id).text('Edit')