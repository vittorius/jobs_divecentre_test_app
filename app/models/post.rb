class Post < ActiveRecord::Base
  belongs_to :author, :class_name => 'User', :foreign_key => 'user_id'
  has_many :comments, :dependent => :destroy

  attr_accessible :header, :text

  validates :header, :text, :user_id, :presence => true
  validates :header, length: { maximum: 50 }
  validates :text, length: { maximum: 500 }

end
