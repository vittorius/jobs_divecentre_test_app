class Role < ActiveRecord::Base
  ADMIN = 'Admin'
  USER = 'User'

  has_and_belongs_to_many :users

  attr_accessible :name

  def self.get(name)
    Role.find_by_name(name)
  end
end
