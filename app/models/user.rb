class User < ActiveRecord::Base
  before_create :add_default_roles

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :name

  has_many :posts, :dependent => :destroy
  has_and_belongs_to_many :roles

  def admin?
    roles(true).any? { |r| r.name == Role::ADMIN }
  end

  def regular?
    roles(true).any? { |r| r.name == Role::USER }
  end

  private
  def add_default_roles
    roles << Role.get(Role::USER)
  end
end
