class CommentsController < ApplicationController
  #noinspection RailsParamDefResolve
  #before_filter :authenticate_user!
  load_and_authorize_resource :post
  load_and_authorize_resource :comment, :through => :post

  def create
    #@post = Post.find(params[:post_id])
    @comment = @post.comments.build(params[:comment])
    @comment.author = current_user
    @comment.save
    redirect_to post_path(@post)
  end

  def update
    #@post = Post.find(params[:post_id])
    #@comment = @post.comments.find(params[:id])
    @comment.update_attributes(comment_params)
    redirect_to post_path(@post)
  end

  def destroy
    #@post = Post.find(params[:post_id])
    #@comment = @post.comments.find(params[:id])
    @comment.destroy
    redirect_to post_path(@post)
  end

  private

  def comment_params
    params.require(:comment).permit(:text)
  end
end