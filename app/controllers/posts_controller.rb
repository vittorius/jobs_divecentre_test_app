class PostsController < ApplicationController
  #before_filter :set_post, :except => [:index, :new, :create]
  #noinspection RailsParamDefResolve
  #before_filter :authenticate_user!, except: [:index, :show]

  load_and_authorize_resource :post

  def index
    @posts = Post.all
  end

  def show
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.author = current_user
    if @post.save
      flash[:success] = "Post created."
      redirect_to @post
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @post.update_attributes(post_params)
      flash[:success] = "Post updated"
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    if @post.destroy
      flash[:success] = "Post deleted"
    else
      flash[:error] = "Error deleting post"
    end
    redirect_to root_url
  end

  private
    #def set_post
    #  @post = Post.find(params[:id])
    #end

    def post_params
      params.require(:post).permit(:header, :text)
    end
end
