FactoryGirl.define do
  factory :user_role, class: Role do
    name Role::USER
  end

  factory :admin_role, class: Role do
    name Role::ADMIN
  end

  factory :user, aliases: [:author] do
    #sequence(:email) { |n| "user#{n}@gmail.com"}
    #sequence(:name) { |n| "User #{n}" }
    email "user1@gmail.com"
    name "User 1"
    password 'password'
    password_confirmation 'password'
    remember_me true
    #roles [role]
  end

  factory :admin, class: User do
    email 'admin@gmail.com'
    name 'Admin'
    password 'password'
    password_confirmation 'password'
    remember_me true
    #roles [admin_role]
  end

  factory :post do
    #sequence(:header) { |n| "Post #{n}"}
    #sequence(:text) { |n| "Content #{n}"}
    header "Post 1"
    text "Content 1"
    #author
  end
end

#admin_role = Role.create({ name: Role::ADMIN })
#Role.create({ name: Role::USER })
#
#u = User.create({
#                    email: 'admin@gmail.com',
#                    name: 'Admin',
#                    password: 'password',
#                    password_confirmation: 'password',
#                    remember_me: true
#                })
#u.roles = [admin_role]
#u.save
#
#u = User.create({
#                    email: 'user@gmail.com',
#                    name: 'User',
#                    password: 'password',
#                    password_confirmation: 'password',
#                    remember_me: true
#                })
#u.save

