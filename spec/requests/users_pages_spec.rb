require 'spec_helper'
require 'support/utilities'

describe "User pages" do
  before {
    create(:admin_role)
    create(:user_role)
  }

  subject { page }

  describe "Signup page" do
    before { visit new_user_registration_path }

    it { should have_content('Sign up') }
  end

  describe "Signup" do
    before do
      visit new_user_registration_path
      fill_in "Email", with: "user@example.com"
      fill_in "Password", with: "foobar12"
      fill_in "Password confirmation", with: "foobar12"
    end

    let(:submit) { "Sign up" }

    describe "with invalid information" do
      it "should not create a user" do
        fill_in "Email", with: ""
        fill_in "Password", with: ""
        fill_in "Password confirmation", with: ""

        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe "with selectively invalid information" do
      shared_examples_for "form with error" do |field, value, msg|
        unless field.nil? || value.nil?
          before do
            fill_in field, with: value
            click_button submit
          end
        end

        it "shows proper error message" do
          should have_content(msg)
        end
      end

      describe "with invalid email" do
        describe "with blank email" do
          it_should_behave_like "form with error", "Email", "", "Email can't be blank"
        end

        describe "with malformed email" do
          it_should_behave_like "form with error", "Email", "test@@", "Email is invalid"
        end
      end

      describe "with invalid password" do
        describe "with password too short" do
          it "shows proper error message" do
            fill_in "Password", with: "foo"
            fill_in "Password confirmation", with: "foo"
            click_button submit
            #  should have_content("Password is too short (minimum is \
            #                      #{User.validators_on(:password).select { |v| v.is_a? ActiveModel::Validations::LengthValidator }.first.options[:minimum]} \
            #characters)")
            should have_content("Password is too short (minimum is #{Devise.password_length.begin} characters)")
          end
          it_should_behave_like "form with error", nil, nil, nil
        end

        describe "with blank password" do
          it_should_behave_like "form with error", "Password", "", "Password can't be blank"
        end
      end
    end

    describe "with valid information" do
      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end

      describe "after saving the user" do
        before { click_button submit }

        it { should have_link('Sign out') }
        it { should have_notice_message('Welcome! You have signed up successfully.') }
      end
    end
  end

  describe "Signin page" do
    before { visit new_user_session_path }
    let(:submit) { 'Sign in' }

    it { should have_content('Sign in') }

    describe "with invalid information" do
      before { click_button submit }

      it { should have_alert_message('Invalid email or password.') }

      describe "after visiting another page" do
        before { click_link "Home" }
        it { should_not have_selector('div.alert.alert-error') }
      end
    end

    describe "with valid information" do
      let(:user1) { create(:user) }
      before { sign_in user1 }

      it { should have_content('Posts') }
      it { should have_link('New post',    href: new_post_path) }
      it { should have_link('Sign out',    href: destroy_user_session_path) }
      it { should_not have_link('Sign in', href: new_user_session_path) }

      describe "followed by signout" do
        before { click_link 'Sign out' }
        it { should have_link('Sign in') }
      end
    end
  end

  describe "Authorization" do
    describe "for non-signed-in users" do
      let(:user1) { create(:user) }

      describe "when attempting to visit a protected page" do
        before do
          visit edit_user_registration_path
          fill_in "Email",    with: user1.email
          fill_in "Password", with: user1.password
          click_button "Sign in"
        end

        describe "after signing in" do
          it "should render the desired protected page" do
            expect(page).to have_content('Edit User')
          end
        end
      end
    end
  end
end